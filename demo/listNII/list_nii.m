function list_nii
clear all, clc
%% pathNII Newton

%25v_6s
pathNII='/home/iperez/ADNI_274v_68s/nii';
nameList='ListNII_iperez_274v_68s';
pathNIILong='/home/iperez/ADNI_274v_68s/Long';
nameListLong='Long_iperez_274v_68s';

% %234v_64s
% pathNII='/media/maxtor2/personal/fsoria/fsoria/ADNI_234v_64s/nii';
% nameList='ListNII_fsoria_234v_64s';
% pathNIILong='/media/maxtor2/personal/fsoria/fsoria/ADNI_234v_64s/Long';
% nameListLong='Long_fsoria_234v_64s';

% %113v_21s
% pathNII='/media/maxtor2/personal/idelacalle/preclinico/IMAGENESYXML/ADNI_bloque_irene/nii';
% nameList='ListNII_idelacalle_113v_21s';
% pathNIILong='/media/maxtor2/personal/idelacalle/preclinico/IMAGENESYXML/ADNI_bloque_irene/Long';
% nameListLong='Long_idelacalle_113v_21s';

%% pathNII Gauss

% %69v_14s
% pathNII='/media/6T/personal/cplatero/preAD/FS/conversores/Sorted/nii';
% nameList='ListNII_preclinical_69v_14s';
% pathNIILong='/media/6T/personal/cplatero/preAD/FS/conversores/Long';
% nameListLong='Long_preclinical_69v_14s';

% %81v_13s
% pathNII='/media/6T/personal/cplatero/preAD/preAD_idelacalle/Sorted/nii';
% nameList='ListNII_preclinical_81v_13s';
% pathNIILong='/media/6T/personal/cplatero/preAD/preAD_idelacalle/Long';
% nameListLong='Long_preclinical_81v_13s';

% %301v_65s
% pathNII='/media/6T/personal/cplatero/preAD/FS/FS/MRI/Sorted/nii';
% nameList='ListNII_preclinical_301v_65s';
% pathNIILong='/media/6T/personal/cplatero/preAD/FS/FS/Long';
% nameListLong='Long_preclinical_301v_65s';

% %603v_111s
% pathNII=''; %'/media/Elements/Gauss/ywu/Sorted/Sorted_NC/NC';
% nameList='ListNII_ywu_603v_111s';
% pathNIILong='/media/Elements/Gauss/ywu/NC/Long';
% nameListLong='Long_ywu_603v_111s';


%% pathNII Copernico

% pathNII='/media/Maxtor/personal/afernandez/ADNI_164V_40S/nii';
% nameList='ListNII_afernandez_164v_40s';
% pathNIILong='/media/Maxtor/personal/afernandez/ADNI_164V_40S/Long';
% nameListLong='Long_afernandez_164v_40s';


%% CODE
if isempty(pathNII)
    listNII=dir(pathNIILong);
    listNII=listNII(3:end);
    listname=struct('name',{});
    for i=1:numel(listNII)
        if listNII(i).isdir==1
            name=string(listNII(i).name);
            listname(i).name=extractBefore(name,'long');
        end
    end
    listNII=listname;
else
    listNII=dir(strcat(pathNII,'/*.nii*'));
end

save(strcat(nameList,'.mat'),'listNII');

pathNII=pathNIILong;
listNII=dir(pathNII);
save(strcat(nameListLong,'.mat'),'listNII','pathNII');


end
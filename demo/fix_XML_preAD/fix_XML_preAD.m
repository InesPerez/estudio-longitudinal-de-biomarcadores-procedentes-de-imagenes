function fix_XML_preAD
clc;
%% IMAGEUID
% xmlPath='./idelacalle_113v/xml/';
% xmlTmp='./idelacalle_113v/xml_tmp/';
% xmlResult='./idelacalle_113v/xml_2/';

% xmlPath='./afernandez_164v/xml/';
% xmlTmp='./afernandez_164v/xml_tmp/';
% xmlResult='./afernandez_164v/xml_2/';

% xmlPath='./fsoria_234v/xml/';
% xmlTmp='./fsoria_234v/xml_tmp/';
% xmlResult='./fsoria_234v/xml_2/';

% xmlPath='/home/iperez/ADNI_274v_68s/xml/';
% xmlTmp='./iperez_274v/xml_tmp/';
% xmlResult='./iperez_274v/xml_2/';

% xmlPath='./ywu_603v/new/xml/';
% xmlTmp='./ywu_603v/new/xml_tmp/';
% xmlResult='./ywu_603v/new/xml_2/';
% xmlTmp2='./ywu_603v/new/xml_tmp2/';
% main_path='./ywu_603v/new/';

% xmlPath='./ywu_603v/old/xml/';
% xmlTmp='./ywu_603v/old/xml_tmp/';
% xmlResult='./ywu_603v/old/xml_2/';
% xmlTmp2='./ywu_603v/old/xml_tmp2/';
% main_path='./ywu_603v/old/';

% xmlPath='.\\ywu_603v\\xml\\';
% xmlTmp='.\\ywu_603v\\xml_tmp\\';
% xmlResult='.\\ywu_603v\\xml_2\\';
% xmlTmp2='.\\ywu_603v\\xml_tmp2\\';

% xmlPath='./preclinical/69v_14s/xml/';
% xmlTmp='./preclinical/69v_14s/xml_tmp/';
% xmlResult='./preclinical/69v_14s/xml_2/';

xmlPath='./preclinical/81v_13s/xml/';
xmlTmp='./preclinical/81v_13s/xml_tmp/';
xmlResult='./preclinical/81v_13s/xml_2/';

% xmlPath='./preclinical/301v_65s/xml/';
% xmlTmp='./preclinical/301v_65s/xml_tmp/';
% xmlResult='./preclinical/301v_65s/xml_2/';

%% NII
% niiPath ='./nii/';
% listNII=dir(strcat(niiPath,'*.nii'));

% Path's lists from data not saved in the same computer

% load('./idelacalle_113v/ListNII_idelacalle_113v_21s','listNII');
% load('./afernandez_164v/ListNII_afernandez_164v_40s','listNII');
% load('./fsoria_234v/ListNII_fsoria_234v_64s','listNII');
% load('./iperez_274v/ListNII_iperez_274v_68s','listNII');
% load('./ywu_603v/ListNII_ywu_603v_111s','listNII');
% load('./preclinical/ListNII_preclinical_301v_65s','listNII');
load('./preclinical/ListNII_preclinical_81v_13s','listNII');
% load('./preclinical/ListNII_preclinical_69v_14s','listNII');

%% Change names
% A list of all xml files is generated
listFichXML = dir(strcat(xmlPath,'*.xml'));
numXML = numel(listFichXML);

UID_orig=[];
UID_dest=[];

for i=1:numXML
   IMAGEUID_filename=char(extractBetween(listFichXML(i).name,'_I','.'));
   IMAGEUID_XML=readXML_IMAGEUID(strcat(xmlPath,listFichXML(i).name));

   if(strcmp(IMAGEUID_filename,IMAGEUID_XML))
       orig=strcat(xmlPath,listFichXML(i).name);
       command=sprintf('cp %s %s',orig,xmlResult);
       system(command);
   else
       fprintf('%s -- %s \n',IMAGEUID_filename,IMAGEUID_XML);
       UID_orig=[UID_orig;listFichXML(i).name,32];
       orig=strcat(xmlPath,listFichXML(i).name);
       if(length(IMAGEUID_XML)==6 && length(IMAGEUID_filename)==6)
         dest=strcat(xmlTmp,listFichXML(i).name(1:22),IMAGEUID_XML,'.xml');
         UID_dest=[UID_dest;strcat(listFichXML(i).name(1:22),IMAGEUID_XML,'.xml')];
       elseif(length(IMAGEUID_XML)==6 && length(IMAGEUID_filename)==5)
         dest=strcat(xmlTmp,listFichXML(i).name(1:20),'_I',IMAGEUID_XML,'.xml');
         UID_dest=[UID_dest;strcat(listFichXML(i).name(1:20),'_I',IMAGEUID_XML,'.xml')];
       elseif(length(IMAGEUID_XML)==5 && length(IMAGEUID_filename)==6)
         dest=strcat(xmlTmp,listFichXML(i).name(1:20),'__I',IMAGEUID_XML,'.xml');
         UID_dest=[UID_dest;strcat(listFichXML(i).name(1:20),'__I',IMAGEUID_XML,'.xml')];
       elseif(length(IMAGEUID_XML)==5 && length(IMAGEUID_filename)==5)
         %without debbuging
         dest=strcat(xmlTmp,listFichXML(i).name(1:23),IMAGEUID_XML,'.xml');
         UID_dest=[UID_dest;strcat(listFichXML(i).name(1:23),IMAGEUID_XML,'.xml')];
       end
           
       command=sprintf('cp %s %s',orig,dest);
       disp(command);
       system(command);
   end

end

%% code_v
UID_orig_s=string(UID_orig);
UID_orig_s=extractBetween(UID_orig_s,'_I','.');
UID_dest_s=string(UID_dest);
UID_dest_s=extractBetween(UID_dest_s,'_I','.');

for i=1:size(UID_dest,1)
    index=find(strcmp(UID_orig_s,UID_dest_s(i)));
    
    if(~strcmp(UID_orig(index,1:15),UID_dest(i,1:15)))
        fprintf(2,'Error: changes between XMLs must be the same subject: orig %s, dest %s.\n',...
            UID_orig(index,1:15),UID_dest(i,1:15));
    else
        code_v=UID_orig(index,:);
        new_IMAGEUID=UID_dest(i,:);

        if(length(char(UID_dest_s(i)))==6)
            code_v=code_v(17:20);   
            new_IMAGEUID(17:20)=code_v;
        elseif(length(char(UID_dest_s(i)))==5)
            code_v=code_v(17:21);   
            new_IMAGEUID(17:21)=code_v;
        end

        orig=strcat(xmlTmp,UID_dest(i,:));
        dest=strcat(xmlResult,new_IMAGEUID);
        command=sprintf('cp %s %s',orig,dest);
        disp(command);
        system(command);
    end
    
end


%% Checking NII
clc;
nii_names={listNII(:).name}';
nii_names=extractBefore(string(nii_names),'.');


listFichXML = dir(strcat(xmlResult,'*.xml'));
xml_names=char(listFichXML(:).name);
xml_names_NII=extractBetween(string(xml_names),'ADNI_','.');

for i=1:length(xml_names)
    index=find(strcmp(nii_names,xml_names_NII(i)));
    if(isempty(index))
        orig=strcat(xmlResult,xml_names(i,:));
        dest=strcat(xmlTmp2,xml_names(i,:));
        command=sprintf('mv %s %s',orig,dest);
        disp(command);
        system(command);
    end
end

listFichXML = dir(strcat(xmlResult,'*.xml'));
xml_names=char(listFichXML(:).name);
xml_names=extractBetween(string(xml_names),'ADNI_','.');


for i=1:length(xml_names)
    index=find(strcmp(nii_names,xml_names(i)));
    if(length(index)~=1)
        fprintf(2,'%s xml without nii file\n',xml_names(i));
    end
end

if(numel(xml_names)~=numel(nii_names))
    fprintf(2,'The number of files between nii and xml are differents.\n');
end

error_xml=[];
n=0;
for i=1:length(nii_names)
    index=find(strcmp(xml_names,nii_names(i)));
    if(length(index)~=1)
        fprintf(2,'%s nii without xml file\n',nii_names(i));
        aux=extractAfter(nii_names(i),'_I');
        error_xml=strcat(error_xml,aux,',');
        n=n+1;
    end
end

if ~isempty(error_xml)
    fprintf('%f nii files need .xml file.\n',n);
    save(strcat(main_path,'error_xml.mat'),'error_xml','n');
end

end


function IMAGEUID=readXML_IMAGEUID(filename)
% This function is charged of finding the different parameters in the xml
% file, when possible. Default value for all of them is -1, and only if a
% valid value is found it will be changed.

%% Reading the xml file

theStruct = xml2struct(filename);

% These parts of the struct are repeatedly used over the function.
data1 = theStruct.idaxs.project.subject;

%% Guaranteed parameters
% These parameters are always expected to be found, and so they're not
% checked.

%% sex, id, diagnose, age
IMAGEUID = data1.study.series.seriesLevelMeta.derivedProduct.imageUID.Text;
%imageUID=str2double(data1.study.series.seriesLevelMeta.derivedProduct.imageUID.Text);
end
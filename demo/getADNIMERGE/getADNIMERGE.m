clear all,clc

% Tested in Matlab 2019b

%% ADNIMERGE
options=detectImportOptions('ADNIMERGE.csv');%It converts numerical and datetime data.
%index=strcmp(options.VariableTypes,'char');
%options.VariableTypes(index)={'categorical'};
ADNIMERGE=readtable('ADNIMERGE.csv',options);
ADNIMERGE=convertvars(ADNIMERGE,@iscell,'categorical');%Since 2018b

var=size(ADNIMERGE);
var=var(1);
ptid=unique(ADNIMERGE.PTID);
v=size(ADNIMERGE);
save(strcat('ADNIMERGE_',string(v(1)),'v_',string(length(ptid)),'s.mat'),'ADNIMERGE');

date=ADNIMERGE.update_stamp;
date = sortrows(date,-1);
date=char(date(1));
save('date.mat','date');

%% DICTIONARY
options_dic=detectImportOptions('ADNIMERGE.csv');
% index_d=strcmp(options_dic.VariableTypes,'char');
% options_dic.VariableTypes(index_d)={'categorical'};
% ADNIMERGE_DICT=readtable('ADNIMERGE_DICT.csv',options_dic);
ADNIMERGE_DICT=convertvars(ADNIMERGE_DICT,@iscell,'categorical');%Since 2018b
save('ADNIMERGE_DICTIONARY.mat','ADNIMERGE_DICT');

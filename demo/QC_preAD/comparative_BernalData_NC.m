 function comparative_BernalData_NC
addpath('./external/lme/univariate'); %lme_lowessPlot
clc;
close all;

%% data
% PreAD
%load('./data/Tresults_1548v_326s', 'Tresults');
load('./data/Tresults_1822v_394s_mri', 'Tresults');

% Bernal
load('./data/ADNI791_Hipp_and_Entorh.mat','X_Hipp','Y','group','ni');


%% Checking NHV and ECT in relation to Bernal data
NHV_all=(Y(:,1)+Y(:,2))./Y(:,3)*1e3;
ECT_all=(Y(:,4)+Y(:,5))/2;
group_all=group;

mask_NC_Bernal=(group==1) ;
NHV=NHV_all(mask_NC_Bernal);
ECT=ECT_all(mask_NC_Bernal);

group=group_all(mask_NC_Bernal)+1;
time_BS_Bernal=X_Hipp(mask_NC_Bernal,2);

NHV_Check =(Tresults.LHippVol+Tresults.RHippVol)./Tresults.ICV*1e3;
ECT_Check =(Tresults.L_ECT+Tresults.R_ECT)/2;

gr_check=double(Tresults.Convert);
gr_check_all=gr_check;
gr_check_all(:)=5;

figure(1);

subplot(2,2,1);lme_lowessPlot([Tresults.years;Tresults.years;time_BS_Bernal],...
    [NHV_Check;NHV_Check;NHV],.95,[gr_check;gr_check_all;group]);
title('Smoothed mean measurement trajectories');
ylabel('Normalized hippocampal volume');
xlabel('Time from baseline (in years)');
legend('sNC_{our}','pNC_{our}','NC_{Bernal}','NC_{our}');


technology=FLDstregnth_group(Tresults);
no_undef=technology~=0;
gr_check_tech(gr_check==0 & technology==1,:)=0;%sNC 1.5 Tesla
gr_check_tech(gr_check==1 & technology==1,:)=1;%pNC 1.5 Tesla
gr_check_tech(gr_check==0 & technology==2,:)=2;%sNC 3 Tesla
gr_check_tech(gr_check==1 & technology==2,:)=3;%pNC 3 Tesla
gr_check_tech=gr_check_tech(no_undef);
subplot(2,2,3);lme_lowessPlot(Tresults.years(no_undef),...
    NHV_Check(no_undef,:),.95,gr_check_tech);
title('Smoothed mean measurement trajectories according MRI Technology');
ylabel('Normalized hippocampal volume');
xlabel('Time from baseline (in years)');
legend('sNC 1.5T','pNC 1.5T','sNC 3T','pNC 3T');

subplot(2,2,2);lme_lowessPlot([Tresults.years;Tresults.years;time_BS_Bernal],...
    [ECT_Check;ECT_Check;ECT],.8,[gr_check;gr_check_all;group]);
title('Smoothed mean measurement trajectories');
ylabel('ECT [mm]');
xlabel('Time from baseline (in years)');
legend('sNC_{our}','pNC_{our}','NC_{Bernal}','NC_{our}');

subplot(2,2,4);lme_lowessPlot(Tresults.years(no_undef),...
    ECT_Check(no_undef),.8,gr_check_tech);
title('Smoothed mean measurement trajectories according MRI Technology');
ylabel('ECT [mm]');
xlabel('Time from baseline (in years)');
legend('sNC 1.5T','pNC 1.5T','sNC 3T','pNC 3T');

figure(2)
Tr_y=Tresults(no_undef,:);
pCN_3t=gr_check_tech==3;
pCN_3t_year=groups_time(Tr_y(pCN_3t,:),6);

NHV_Check_y=NHV_Check(no_undef,:);
subplot(1,2,1);lme_lowessPlot(Tr_y.years(pCN_3t),...
    NHV_Check_y(pCN_3t,:),.8,pCN_3t_year);
title('Smoothed mean measurement trajectories in pCN 3T subjects');
ylabel('Normalized hippocampal volume');
xlabel('Time from baseline (in years)');
legend('until month 6','others');

ECT_Check_y=ECT_Check(no_undef,:);
subplot(1,2,2);lme_lowessPlot(Tr_y.years(pCN_3t),...
    ECT_Check_y(pCN_3t,:),.8,pCN_3t_year);
title('Smoothed mean measurement trajectories in pCN 3T subjects');
ylabel('ECT [mm]');
xlabel('Time from baseline (in years)');
legend('until month 6','others');

figure(3)
pCN_1t=gr_check_tech==1;
pCN_1t_year=groups_time(Tr_y(pCN_1t,:),36);
lme_lowessPlot(Tr_y.years(pCN_1t),...
    ECT_Check_y(pCN_1t,:),.8,pCN_1t_year);
title('Smoothed mean measurement trajectories in pCN 1.5T subjects');
ylabel('ECT [mm]');
xlabel('Time from baseline (in years)');
legend('until year 3','others');

%% Testing atrophy
time_BS_Bernal=X_Hipp(mask_NC_Bernal,2);
ni_Bernal=get_ni(time_BS_Bernal,[]);
BlAge=X_Hipp(mask_NC_Bernal,12);
atrophy_Bernal_NHV_NC=getAtrophy([time_BS_Bernal,BlAge],ni_Bernal,...
    NHV_all(mask_NC_Bernal));
atrophy_Bernal_ECT_NC=getAtrophy([time_BS_Bernal,BlAge],ni_Bernal,...
    ECT_all(mask_NC_Bernal));


mask_NC_Check= gr_check==0; 
time_BS_Check=Tresults.years(mask_NC_Check);
ni_Check=get_ni(time_BS_Check,[]);
BlAge=Tresults.Age(mask_NC_Check)-time_BS_Check;
atrophy_Check_NHV_sNC=getAtrophy([time_BS_Check,BlAge],ni_Check,...
    NHV_Check(mask_NC_Check));
atrophy_Check_ECT_sNC=getAtrophy([time_BS_Check,BlAge],ni_Check,...
    ECT_Check(mask_NC_Check));

mask_NC_Check= gr_check==1; 
time_BS_Check=Tresults.years(mask_NC_Check);
ni_Check=get_ni(time_BS_Check,[]);
BlAge=Tresults.Age(mask_NC_Check)-time_BS_Check;
atrophy_Check_NHV_pNC=getAtrophy([time_BS_Check,BlAge],ni_Check,...
    NHV_Check(mask_NC_Check));
atrophy_Check_ECT_pNC=getAtrophy([time_BS_Check,BlAge],ni_Check,...
    ECT_Check(mask_NC_Check));


clc;
fprintf('NHV\n');
fprintf('Baseline Bernal NHV: %.2f %.2f (NC)\n',...
    mean(atrophy_Bernal_NHV_NC{1}),std(atrophy_Bernal_NHV_NC{1}));

fprintf('Baseline Check  NHV: %.2f %.2f (sNC) %.2f %.2f(pNC)\n',...
    mean(atrophy_Check_NHV_sNC{1}),std(atrophy_Check_NHV_sNC{1}),...
    mean(atrophy_Check_NHV_pNC{1}),std(atrophy_Check_NHV_pNC{1}));

fprintf('Atrophy  Bernal NHV: %.2f %.2f (NC)\n',...
    mean(atrophy_Bernal_NHV_NC{2}),std(atrophy_Bernal_NHV_NC{2}));

fprintf('Atrophy  Check  NHV: %.2f %.2f (sNC) %.2f %.2f(pNC)\n',...
    mean(atrophy_Check_NHV_sNC{2}),std(atrophy_Check_NHV_sNC{2}),...
    mean(atrophy_Check_NHV_pNC{2}),std(atrophy_Check_NHV_pNC{2}));


fprintf('ECT\n');
fprintf('Baseline Bernal ECT: %.2f %.2f (NC)\n',...
    mean(atrophy_Bernal_ECT_NC{1}),std(atrophy_Bernal_ECT_NC{1}));

fprintf('Baseline Check  ECT: %.2f %.2f (sNC) %.2f %.2f(pNC)\n',...
    mean(atrophy_Check_ECT_sNC{1}),std(atrophy_Check_ECT_sNC{1}),...
    mean(atrophy_Check_ECT_pNC{1}),std(atrophy_Check_ECT_pNC{1}));

fprintf('Atrophy  Bernal ECT: %.2f %.2f (NC)\n',...
    mean(atrophy_Bernal_ECT_NC{2}),std(atrophy_Bernal_ECT_NC{2}));

fprintf('Atrophy  Check  ECT: %.2f %.2f (sNC) %.2f %.2f(pNC)\n',...
    mean(atrophy_Check_ECT_sNC{2}),std(atrophy_Check_ECT_sNC{2}),...
    mean(atrophy_Check_ECT_pNC{2}),std(atrophy_Check_ECT_pNC{2}));


figure(4);
subplot(2,2,1);histogram(atrophy_Bernal_NHV_NC{1});hold on;
histogram(atrophy_Check_NHV_sNC{1});hold off;
title('Baseline NHV sNC');
subplot(2,2,3);histogram(atrophy_Bernal_NHV_NC{2});hold on;
histogram(atrophy_Check_NHV_sNC{2});hold off;
title('Atrophy NHV sNC');

subplot(2,2,2);histogram(atrophy_Bernal_NHV_NC{1});hold on;
histogram(atrophy_Check_NHV_pNC{1});hold off;
title('Baseline NHV pNC');

subplot(2,2,4);histogram(atrophy_Bernal_NHV_NC{2});hold on;
histogram(atrophy_Check_NHV_pNC{2});hold off;
title('Atrophy NHV pNC');

figure(5);

subplot(2,2,1);histogram(atrophy_Bernal_ECT_NC{1});hold on;
histogram(atrophy_Check_ECT_sNC{1});hold off;
title('Baseline ECT sNC');

subplot(2,2,3);histogram(atrophy_Bernal_ECT_NC{2});hold on;
histogram(atrophy_Check_ECT_sNC{2});hold off;
title('Atrophy ECT sNC');

subplot(2,2,2);histogram(atrophy_Bernal_ECT_NC{1});hold on;
histogram(atrophy_Check_ECT_pNC{1});hold off;
title('Baseline ECT pNC');

subplot(2,2,4);histogram(atrophy_Bernal_ECT_NC{2});hold on;
histogram(atrophy_Check_ECT_pNC{2});hold off;
title('Atrophy ECT pNC');




 end

function ni = get_ni(time_visit,numIDs)
baseline=find(time_visit==0);
numScans=length(time_visit);
ni=baseline([2:end,end])-baseline;
ni(end)=numScans-baseline(end)+1;
if(isempty(numIDs)==0)
    if(length(ni)~=numIDs)
        warning('Error calculating ni: There are more scans in baseline than subjects');
    end
end
if(sum(ni)~=numScans)
   warning('Error calculating ni');
end

end


function atrophy=getAtrophy(X,ni,HippMarker)


time=X(:,1);
BlAge=X(:,2);


BlAge_ni=BlAge(time==0);

atrophy=cell(2,1);
intercept=ones(length(time),1);
X_Hipp=[intercept,time];%,BlAge];
model=lme_fit_FS(X_Hipp,[1 2],HippMarker,ni);

% atrophy{1}=model.Bhat(1)+(model.Bhat(3)*BlAge_ni')+model.bihat(1,:);
% atrophy{2}=(((model.Bhat(2)+model.bihat(2,:)))./...
%     ((model.Bhat(1)+(model.Bhat(3)*BlAge_ni')+model.bihat(1,:)))*100);

atrophy{1}=model.Bhat(1)+model.bihat(1,:);
atrophy{2}=(((model.Bhat(2)+model.bihat(2,:)))./...
    ((model.Bhat(1)+model.bihat(1,:)))*100);
end

function technology=FLDstregnth_group(Tresults)
    FLDSTRENG=Tresults.FLDSTRENG;
    RID=unique(Tresults.RID);
    technology=zeros(length(FLDSTRENG),1);
    year=zeros(length(FLDSTRENG),1);% subjects with measures until year 1
    index_y=[];
    no_pcn_tech_b=[];
    for i=1:numel(RID)
        index=find(Tresults.RID==RID(i));
        tech_a=FLDSTRENG(index)=='1.5 Tesla MRI';
        tech_b=FLDSTRENG(index)=='3 Tesla MRI';
        if ~(sum(tech_a)~=0 && sum(tech_b)~=0) % if no different technologies
            technology(index(tech_a))=1;
            technology(index(tech_b))=2;
        end
    end
end

function cohort_ab=groups_time(Tresults,time)
    RID=unique(Tresults.RID);
    cohort_ab=zeros(height(Tresults),1);
    for i=1:numel(RID)
        index=find(Tresults.RID==RID(i));
        threshold=Tresults.M(index)>time;
        if sum(threshold)>0
            cohort_ab(index)=1;
        end
    end
end


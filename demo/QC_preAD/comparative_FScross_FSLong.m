function comparative_FScross_FSLong
% Comparative between ADNIMERGE (cross 4.4) and Freesufer longitudinal
% pipeline (only T1)

%% data

%Pre-clinical
%load('./data/Tresults_1548v_326s', 'Tresults');
load('./data/Tresults_1822v_394s_mri', 'Tresults');


%% Comparative with hippocampal volume and ICV
close all;

figure(1);
subplot(1,2,1);
mask_hippocampus=isnan(Tresults.Hippocampus)==0;
plot(Tresults.Hippocampus(mask_hippocampus),Tresults.LHippVol(mask_hippocampus)+...
    Tresults.RHippVol(mask_hippocampus),'+');
xlabel('Freesurfer cross-sectional  v 4.4');
ylabel('Freesurfer longitudinal  v 5.3');
title('Comparison with Hippocampal volume');
% index=find(Tresults.Hippocampus==3737);
% text(Tresults.Hippocampus(index),Tresults.LHippVol(index)+Tresults.RHippVol(index),...
%     '094\_S\_1293: m24');
% 
% for i=index-3:index
%     fprintf('%.0f %.0f\n',Tresults.Hippocampus(i),Tresults.LHippVol(i)+Tresults.RHippVol(i));
% end
% 


subplot(1,2,2);
mask_ICV=isnan(Tresults.ICV)==0;
plot(Tresults.ICV(mask_ICV),Tresults.ICV_xml(mask_ICV),'+');
xlabel('Freesurfer cross-sectional  v 4.4');
ylabel('Freesurfer longitudinal  v 5.3');
title('Comparison with ICV');
% 1330
% index=find(Tresults.ICV_ADNI<1374e3 & Tresults.ICV_ADNI>1372e3);
% text(Tresults.ICV_ADNI(index),Tresults.ICV(index),...
%     '094\_S\_1293: m24');
% 
% for i=index-3:index
%     fprintf('%.0f %.0f\n',Tresults.ICV_ADNI(i),Tresults.ICV(i));
% end

% T1+DTI MCI to AD
% index=find(abs(Tresults.ICV_ADNI-1.611e6)<1e4 & abs(Tresults.ICV_ADNI-1.316e6)<1e4);
% text(Tresults.ICV_ADNI(index),Tresults.ICV(index),...
%     '094\_S\_1293: m24');
% 
% for i=index-3:index
%     fprintf('%.0f %.0f\n',Tresults.ICV_ADNI(i),Tresults.ICV(i));
% end
% 

figure(2);
mask_Ventricles=isnan(Tresults.Ventricles)==0;
plot(Tresults.Ventricles(mask_Ventricles),Tresults.LLatVentr(mask_Ventricles)+...
    Tresults.RLatVentr(mask_Ventricles),'+');
xlabel('Freesurfer cross-sectional  v 4.4');
ylabel('Freesurfer longitudinal  v 5.3');
title('Comparison with Ventricle volume');

end
function [Tresults,ni,corr_QC,vol_QC]=join_QC_preAD

%% data

% 274v_68s
load('./data/274v_68s/Tresults_274v_68s_mri', 'Tresults');
load('./data/274v_68s/QC_long_preAD_274v_68s','corr_QC','vol_QC');
T_274v_68s=Tresults;
v_274v=vol_QC;
c_68s=corr_QC;
ni_68s=get_ni(Tresults.years,length(unique(Tresults.RID)));

% 234v_64s
load('./data/234v_64s/new/Tresults_234v_64s_mri', 'Tresults');
load('./data/234v_64s/new/QC_long_preAD_234v_64s','corr_QC','vol_QC');
T_234v_64s=Tresults;
v_234v=vol_QC;
c_64s=corr_QC;
ni_64s=get_ni(Tresults.years,length(unique(Tresults.RID)));

% 113v_24s
load('./data/113v_24s/new/Tresults_113v_24s_mri', 'Tresults');
load('./data/113v_24s/new/QC_long_preAD_113v_24s','corr_QC','vol_QC');
T_113v_24s=Tresults;
v_113v=vol_QC;
c_24s=corr_QC;
ni_24s=get_ni(Tresults.years,length(unique(Tresults.RID)));

% 147v_35s
load('./data/147v_35s/new/Tresults_147v_35s_mri', 'Tresults');
load('./data/147v_35s/new/QC_long_preAD_147v_35s','corr_QC','vol_QC');
load('./data/147v_35s/new/corr_QC_147v_old','corr_QC_old');
T_147v_35s=Tresults;
v_147v=vol_QC;
c_35s=corr_QC_old;%corr_QC;
ni_35s=get_ni(Tresults.years,length(unique(Tresults.RID)));

% 69v_14s
load('./data/69v_14s/new/Tresults_69v_14s_mri', 'Tresults');
load('./data/69v_14s/new/QC_long_preAD_conver_69v_14s','corr_QC','vol_QC');
T_69v_14s=Tresults;
v_69v=vol_QC;
c_14s=corr_QC;
ni_14s=get_ni(Tresults.years,length(unique(Tresults.RID)));

% 81v_13s
load('./data/81v_13s/new/Tresults_81v_13s_mri', 'Tresults');
load('./data/81v_13s/new/QC_long_preAD_81v_13s','corr_QC','vol_QC');
T_81v_13s=Tresults;
v_81v=vol_QC;
c_13s=corr_QC;
ni_13s=get_ni(Tresults.years,length(unique(Tresults.RID)));

% 301v_65s
load('./data/301v_65s/new/Tresults_301v_65s_mri', 'Tresults');
load('./data/301v_65s/new/QC_long_preAD_301v_65s','corr_QC','vol_QC');
T_301v_65s=Tresults;
v_301v=vol_QC;
c_65s=corr_QC;
ni_65s=get_ni(Tresults.years,length(unique(Tresults.RID)));

% 603v_111s
load('./data/603v_111s/new/Tresults_603v_111s_mri', 'Tresults');
load('./data/603v_111s/new/QC_long_preAD_603v_111s','corr_QC','vol_QC');
T_603v_111s=Tresults;
v_594v=vol_QC;
c_111s=corr_QC;
ni_111s=get_ni(Tresults.years,length(unique(Tresults.RID)));

Tnew=[T_69v_14s;T_301v_65s;T_81v_13s;T_113v_24s;T_234v_64s;T_147v_35s;T_603v_111s;T_274v_68s];
vol_QC=[v_69v;v_301v;v_81v;v_113v;v_234v;v_147v;v_594v;v_274v];
corr_QC=[c_14s;c_65s;c_13s;c_24s;c_64s;c_35s;c_111s;c_68s];
ni=[ni_14s;ni_65s;ni_13s;ni_24s;ni_64s;ni_35s;ni_111s;ni_68s];


%% Checking
if((sum(ni)~=size(Tnew,1)) || (size(Tnew,1)~=size(vol_QC,1)) ||...
        (length(ni)~=size(corr_QC,1)))
    fprintf(2,'Error in table sizes.\n');
end

load('./data/Tresults_1548v_326s','Tresults');
%load('./data/Tresults_1822v_394s_mri','Tresults');

for i=1:size(Tresults,1)
    if(~strcmp(string(Tresults.fsidbase(i,:)),string(Tnew.fsidbase(i,:))))% como eran categorical matlab entendia que eran diferentes
        fprintf(2,'%d %s %s with different subjects\n',i,string(Tresults.fsidbase(i,:)),...
            string(Tnew.fsidbase(i,:)));
    end
end

for i=1:size(Tresults,1)
    if(~strcmp(string(Tresults.fsid(i,:)),string(Tnew.fsid(i,:))))
        fprintf('%d %s %s does not ordered visits\n',i,string(Tresults.fsid(i,:)),...
            string(Tnew.fsid(i,:)));
    end
end

Tresults=Tnew;

end
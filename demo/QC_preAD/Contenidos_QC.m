% Contenidos del control de calidad
% Este ejemplo utiliza comparación entre sujetos NC vs NC_converters
%
% 1. QC_FS_ADNI: Calcula las medidas de intensidad y etiquetamiento con las imágenes del sujeto.
%    Leer las imágenes del sujeto y calcular la correlación de intensidad
%    y el dice de los etiquetamientos. Utiliza QC_FS_ADNI.m.
%    Ejecutable desde linux, ya que emplea Freesufer Subject Dir.
%
% 2. QC_corr_dice_scans: Leer las medidas de correlación y dice de QC_FS_ADNI.m y de Tresults
%    procedente de aseg y aparc. Utiliza QC_corr_dice_scans.m
%    Visualiza en Fig. 1 medidas entre sujetos y en Fig. 2 entre visitas.
%
% 3. comparative_FScross_FSLong: Comparativa entre volumetría hipocampal e ICV entre FS cross-sectional
%    v4.4 (dada por ADNIMERGE) y FS longitudinal desde Tresults.
%    Utiliza comparative_FScross_FSLong.m
%
% 4. comparative_BernalData_NC: Comparativa entre los marcadores de volumen hipocampal normalizado y
%    espesor de la corteza entorrinal dados por Bernal y los calculados por
%    el procesamiento longitudinal nuestro. Se muestra las trayectorias
%    de los dos marcadores y los valores medios al inicio del estudio y
%    la atrofia anual. Utiliza comparative_BernalData_NC.m
%

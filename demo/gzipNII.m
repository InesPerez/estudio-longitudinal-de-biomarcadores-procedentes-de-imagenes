pathNII='../../249v_62s/nii/';
NIIList=dir(strcat(pathNII,'/*.nii'));

% The .nii file is compressed in order to save disk space.
for i=1:numel(NIIList)
    comand=sprintf('gzip -9 %s/%s',pathNII,NIIList(i).name);
    disp(comand);
    system(comand);
end
pathNII='../nii/';
NIIList=dir(strcat(pathNII,'/*.nii.gz'));

% The .nii.gz file is uncompressed in order to use the .nii file
for i=1:numel(NIIList)
    comand=sprintf('gunzip %s/%s',pathNII,NIIList(i).name);
    system(comand);
end
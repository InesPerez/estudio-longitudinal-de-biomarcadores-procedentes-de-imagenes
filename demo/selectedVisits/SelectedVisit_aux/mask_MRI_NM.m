function ADNI_MRI_NM = mask_MRI_NM(ADNI)

RID=unique(ADNI.RID);
mask=false(size(ADNI,1),1);
for i=1:length(RID)
    idx=find(ADNI.RID==RID(i));
    if((sum(~isnan(ADNI.ICV(idx)))>0) && (sum(~isnan(ADNI.FAQ(idx)))>0) &&...
       (sum(~isnan(ADNI.ADAS13(idx)))>0) && (sum(~isnan(ADNI.RAVLT_immediate(idx)))>0))
        mask(idx)=true;
    else
        subj=ADNI(idx,[25,28,35,60]);
        fprintf('Subject %d and %d visits without MRI-markers or NM\n',RID(i),length(idx));
    end
   
end

ADNI_MRI_NM=ADNI(mask,:);


end
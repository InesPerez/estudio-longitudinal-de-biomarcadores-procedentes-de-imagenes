function index=changeVariableADNIMERGE(ADNI_new,ADNI_old)

index=zeros(size(ADNI_new,1),1);
k=1;
for i=1:size(ADNI_new,1)
    idx_tmp=find((ADNI_old.RID==ADNI_new.RID(i)) & ...
        (ADNI_old.M==ADNI_new.M(i)));

    if(length(idx_tmp)==1)
        index(i)=idx_tmp;
    elseif(isempty(idx_tmp))
        fprintf('%d: This visit  %d from RID %d does not found.\n',k,ADNI_new.RID(i),ADNI_new.M(i));
        k=k+1;
    else
        fprintf('Problem with RID %d and %d visit.\n',ADNI_new.RID(i),ADNI_new.M(i));
    end
end

%% Change variable
%new_data=ADNI_old.FLDSTRENG(index(index>0));

end
% This code checks if the input Tresults is included in the working Tresult
% for CN baseline Diagnose

function visits=visits_included(Tresults_check)
clc;clear all
addpath('./data');
if nargin<1
    Tresults_check='Tresults_NC_AD_1617d.mat';
end
load(Tresults_check,'Tresults');

% Deleting AD Subjects
fsidb=string(Tresults.fsidbase);
index=Tresults.DX=='Dementia';
fsidb_ad=unique(fsidb(index));

index_ad=[];
for i=1:numel(fsidb_ad)
   subj_convert=find(strcmp(fsidb,fsidb_ad(i)));
   index_ad=[index_ad;subj_convert];
end
Tresults(index_ad,:)=[];

Tresults_check=Tresults;
load('Tresults_4113v_394s.mat','Tresults');

% Checking IMAGEUID
IMAGEUID_check=str2double(extractAfter(Tresults_check.fsid,'_I'));

n=0;
visits=[];
numImg=numel(IMAGEUID_check);
for i=1:numImg
    img=find(Tresults.IMAGEUID==IMAGEUID_check(i));
    img_xml=find(Tresults.IMAGEUID_xml==IMAGEUID_check(i));
    if img+img_xml==0
        fprintf('Visit %d not included, fsid: %s.\n',i,char(Tresults_check.fsid(i)));
        n=n+1;
        visits=[visits;Tresults_check(i,:)];
    end
end

fprintf('Total visits not included: %d.\n',n);
pause

function Tdec=joint_AsegAparc_preAD


%% Read demographic and clinical data

%V603 (Long ywu) 
fdemographic='./data/603v_111s/new/clinical_NC_Long.dat';
faseg='./data/603v_111s/new/aseg_long.clinical_NC_Long.dat';
faparc_lh='./data/603v_111s/new/aparc_long_lh.clinical_NC_Long.dat';
faparc_rh='./data/603v_111s/new/aparc_long_rh.clinical_NC_Long.dat';
Tdec=readClinicalData(fdemographic);
TMarkers=convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
T603=[Tdec,TMarkers];

%V301
fdemographic='./data/preclinical_451v/301v_65s/new/clinicaldata_long_301v_65s_NC.dat';
faseg='./data/preclinical_451v/301v_65s/new/aseg_long.clinicaldata_long_301v_65s_NC.dat';
faparc_lh='./data/preclinical_451v/301v_65s/new/aparc_long_lh.clinicaldata_long_301v_65s_NC.dat';
faparc_rh='./data/preclinical_451v/301v_65s/new/aparc_long_rh.clinicaldata_long_301v_65s_NC.dat';
Tdec=readClinicalData(fdemographic);
TMarkers=convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
T301=[Tdec,TMarkers];
% 
%V69
fdemographic='./data/preclinical_451v/69v_14s/new/clinicaldata_long_conversNC.dat';
faseg='./data/preclinical_451v/69v_14s/aseg_long.clinicaldata_long_conversNC.dat';
faparc_lh='./data/preclinical_451v/69v_14s/aparc_long_lh.clinicaldata_long_conversNC.dat';
faparc_rh='./data/preclinical_451v/69v_14s/aparc_long_rh.clinicaldata_long_conversNC.dat';
Tdec=readClinicalData(fdemographic);
TMarkers=convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
T69=[Tdec,TMarkers];

%V81
fdemographic='./data/preclinical_451v/81v_13s/new/clinicaldata_long_preAD_81v_13s.dat';
faseg='./data/preclinical_451v/81v_13s/new/aseg_long.clinicaldata_long_preAD_81v_13s.dat';
faparc_lh='./data/preclinical_451v/81v_13s/new/aparc_long_lh.clinicaldata_long_preAD_81v_13s.dat';
faparc_rh='./data/preclinical_451v/81v_13s/new/aparc_long_rh.clinicaldata_long_preAD_81v_13s.dat';
Tdec=readClinicalData(fdemographic);
TMarkers=convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
T81=[Tdec,TMarkers];

%V113
fdemographic='./data/113v_65s/new/idelacalle_prec.dat';
faseg='./data/113v_65s/new/aseg_long.idelacalle_prec.dat';
faparc_lh='./data/113v_65s/new/aparc_long_lh.idelacalle_prec.dat';
faparc_rh='./data/113v_65s/new/aparc_long_rh.idelacalle_prec.dat';
Tdec=readClinicalData(fdemographic);
TMarkers=convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
T113=[Tdec,TMarkers];
 
%V234
fdemographic='./data/234v_64s/new/clinicaldata_long_234v_64s_NC.dat';
faseg='./data/234v_64s/new/aseg_long.clinicaldata_long_234v_64s_NC.dat';
faparc_lh='./data/234v_64s/new/aparc_long_lh.clinicaldata_long_234v_64s_NC.dat';
faparc_rh='./data/234v_64s/new/aparc_long_rh.clinicaldata_long_234v_64s_NC.dat';
Tdec=readClinicalData(fdemographic);
TMarkers=convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
T234=[Tdec,TMarkers];
 
%V147
fdemographic='./data/147v_35s/new/clinicaldata_long_164v_40s_NC.dat';
faseg='./data/147v_35s/new/aseg_long.clinicaldata_long_164v_40s_NC.dat';
faparc_lh='./data/147v_35s/new/aparc_long_lh.clinicaldata_long_164v_40s_NC.dat';
faparc_rh='./data/147v_35s/new/aparc_long_rh.clinicaldata_long_164v_40s_NC.dat';
Tdec=readClinicalData(fdemographic);
TMarkers=convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
T147=[Tdec,TMarkers];
mask_subjs=[30;31;32;89;90;91;92;93;94;95;96;130;131;132;158;159;160];
T147(mask_subjs,:)=[];

%V274
fdemographic='./data/274v_68s/new/clinicaldata_long_274v_68s_NC.dat';
faseg='./data/274v_68s/new/aseg_long.clinicaldata_long_274v_68s_NC.dat';
faparc_lh='./data/274v_68s/new/aparc_long_lh.clinicaldata_long_274v_68s_NC.dat';
faparc_rh='./data/274v_68s/new/aparc_long_rh.clinicaldata_long_274v_68s_NC.dat';
Tdec=readClinicalData(fdemographic);
TMarkers=convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
T274=[Tdec,TMarkers];

%% all
Tdec=[T69;T301;T81;T113;T234;T147;T603;T274];

end



function Tdec=readClinicalData(fname)

Qdec = fReadQdec(fname);
Tdec=cell2table(Qdec(2:end,:));
varname=cellstr(Qdec(1,:));
varname{2}='fsidbase';
Tdec.Properties.VariableNames=varname;
Tdec.years=str2double(Tdec.years);
Tdec.Age=str2double(Tdec.Age);
Tdec.MMSE=str2double(Tdec.MMSE);
Tdec.GDS=str2double(Tdec.GDS);
Tdec.CDR=str2double(Tdec.CDR);
Tdec.APOE_A1=str2double(Tdec.APOE_A1);
Tdec.APOE_A2=str2double(Tdec.APOE_A2);

%% NEW CODE
Tdec.FAQ=str2double(Tdec.FAQ);
Tdec.EXAMDATE=datetime(Tdec.EXAMDATE);
Tdec.fsidbase=categorical(Tdec.fsidbase);
Tdec.sex=categorical(Tdec.sex);
Tdec.diagnose=categorical(Tdec.diagnose);
Tdec.IMAGEUID=str2double(Tdec.IMAGEUID);

Tdec.fsid=string(Tdec.fsid);
rid=extractBetween(Tdec.fsid,'S_','_');


m12=strcmp(Tdec.CODE,'m12');
m12_rid=unique(rid(m12));
for i=1:length(m12_rid)
    aux=m12_rid{i};
    m12_EXAMDATE=Tdec.EXAMDATE(rid==aux & m12);
    
    if numel(m12_EXAMDATE)>1 %if 2 m12 samples exist and m60 for one rid, the oldest is m72
        older=max(m12_EXAMDATE);
        m72=Tdec.EXAMDATE==older & m12 & rid==aux;
        Tdec.CODE(m72)={'m72'};
    end
end
        
m60=strcmp(Tdec.CODE,'m60');
m60_rid=unique(rid(m60));        
for i=1:length(m60_rid)
    aux=m60_rid{i};
    m60_EXAMDATE=Tdec.EXAMDATE(rid==aux & m60);
        
    if numel(m60_EXAMDATE)>1%if 2 m60 samples exist for one rid, the oldest is m72
        older=max(m60_EXAMDATE);
        m72=Tdec.EXAMDATE==older & m60 & rid==aux;
        Tdec.CODE(m72)={'m72'};
        
    elseif numel(m60_EXAMDATE)==1 %if 1 m60 exists for one rid but m72 does not exist, check previous EXAMDATE
        index=find(Tdec.EXAMDATE==m60_EXAMDATE & rid==aux);
        reference=Tdec.EXAMDATE(index-1);
        diff=m60_EXAMDATE-reference;
        m72=index;
        if diff>hours((60+6-48)*30*24) && strcmp(Tdec.CODE(index-1),'m48')%6 months margin
            Tdec.CODE(m72)={'m72'};
        elseif diff>hours((60+6-36)*30*24) && strcmp(Tdec.CODE(index-1),'m36')
            Tdec.CODE(m72)={'m72'};
        elseif diff>hours((60+6-24)*30*24) && strcmp(Tdec.CODE(index-1),'m24')
            Tdec.CODE(m72)={'m72'};
        elseif diff>hours((60+6-12)*30*24) && strcmp(Tdec.CODE(index-1),'m12')
            Tdec.CODE(m72)={'m72'};
        end
    end
end

Tdec.CODE=categorical(Tdec.CODE);

var=size(Tdec);
for i=1:var(2)
    column=Tdec{:,i};
    numeric=isnumeric(column);
    if numeric
        NULL=column==-1;
        column(NULL)=NaN;
        Tdec{:,i}=column;
    end
end
end


function TMarkers=convertAsegAparc2Table(fname_aseg,fname_aparc_lh,fname_aparc_rh)

[aseg,~,asegcols] = fast_ldtable(fname_aseg);
asegcols=cellstr(asegcols); % convert column names into cell string
id=[find(strcmp('Left-Hippocampus',asegcols)==1),...
    find(strcmp('Right-Hippocampus',asegcols)==1),...
    find(strcmp('EstimatedTotalIntraCranialVol',asegcols)==1),...
    find(strcmp('lhCortexVol',asegcols)==1),...
    find(strcmp('rhCortexVol',asegcols)==1),...%5
    find(strcmp('Left-Putamen',asegcols)==1),...
    find(strcmp('Right-Putamen',asegcols)==1),...
    find(strcmp('Left-Pallidum',asegcols)==1),...
    find(strcmp('Right-Pallidum',asegcols)==1),...
    find(strcmp('Left-Caudate',asegcols)==1),...%10
    find(strcmp('Right-Caudate',asegcols)==1),...
    find(strcmp('Left-Amygdala',asegcols)==1),...
    find(strcmp('Right-Amygdala',asegcols)==1),...
    find(strcmp('Left-Lateral-Ventricle',asegcols)==1),...   
    find(strcmp('Right-Lateral-Ventricle',asegcols)==1),...%15           
    ];
HV = aseg(:,id);
TAseg=table(HV(:,1),HV(:,2),HV(:,3),HV(:,4),HV(:,5),HV(:,6),HV(:,7),HV(:,8),HV(:,9),HV(:,10),HV(:,11),HV(:,12),HV(:,13),HV(:,14),HV(:,15),...
    'VariableNames',{'LHippVol','RHippVol','ICV','LCortVol','RCortVol','LPutamen','RPutamen','LPallidum','RPallidum','LCaudete','RCaudete','LAmygdala','RAmigdala','LLatVentr','RLatVentr'});

[aparc_lh,~,aparc_lhcols] = fast_ldtable(fname_aparc_lh);

aparc_lhcols=cellstr(aparc_lhcols); % convert column names into cell string
id_lh=[find(strcmp('lh_entorhinal_thickness',aparc_lhcols)==1),...
    find(strcmp('lh_temporalpole_thickness',aparc_lhcols)==1),...
    find(strcmp('lh_inferiortemporal_thickness',aparc_lhcols)==1),...
    find(strcmp('lh_middletemporal_thickness',aparc_lhcols)==1),...
    find(strcmp('lh_inferiorparietal_thickness',aparc_lhcols)==1),...
    find(strcmp('lh_superiorparietal_thickness',aparc_lhcols)==1),...
    find(strcmp('lh_precuneus_thickness',aparc_lhcols)==1),...
    find(strcmp('lh_posteriorcingulate_thickness',aparc_lhcols)==1),...
    find(strcmp('lh_MeanThickness_thickness',aparc_lhcols)==1)];

[aparc_rh,~,aparc_rhcols] =  fast_ldtable(fname_aparc_rh);

 aparc_rhcols=cellstr(aparc_rhcols); % convert column names into cell string           
 id_rh=[find(strcmp('rh_entorhinal_thickness',aparc_rhcols)==1),...
    find(strcmp('rh_temporalpole_thickness',aparc_rhcols)==1),...
    find(strcmp('rh_inferiortemporal_thickness',aparc_rhcols)==1),...
    find(strcmp('rh_middletemporal_thickness',aparc_rhcols)==1),...
    find(strcmp('rh_inferiorparietal_thickness',aparc_rhcols)==1),...
    find(strcmp('rh_superiorparietal_thickness',aparc_rhcols)==1),...
    find(strcmp('rh_precuneus_thickness',aparc_rhcols)==1),...
    find(strcmp('rh_posteriorcingulate_thickness',aparc_rhcols)==1),...
    find(strcmp('rh_MeanThickness_thickness',aparc_rhcols)==1)];

TH=[aparc_lh(:,id_lh),aparc_rh(:,id_rh)];
TApar=table(TH(:,1),TH(:,10),TH(:,9),TH(:,18),TH(:,2),TH(:,3),TH(:,4),TH(:,5),TH(:,6),TH(:,7),TH(:,8),...
    TH(:,11),TH(:,12),TH(:,13),TH(:,14),TH(:,15),TH(:,16),TH(:,17),...
    'VariableNames',{'L_ECT','R_ECT','L_MT','R_MT','LtmpPole','LinfTmp','LmidTmp','LinfPar','LsupPar','Lprecu','LpostCing','RtmpPole','RinfTmp','RmidTmp','RinfPar','RsupPar','Rprecu','RpostCing'});
TMarkers=[TAseg,TApar];


end




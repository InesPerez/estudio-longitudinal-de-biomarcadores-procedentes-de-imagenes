function Tresults=joinNII_ADNIMERGE(n_type)

type_Tresults={'only visits with MRI'};
type=[];

if (nargin==1)
    type=type_Tresults{n_type};
end


addpath('./aux_NII_ADNIMERGE/');
addpath('./external/lme/Qdec/');

%% Tdec and ADNIMERGE data
Tdec=joint_AsegAparc_preAD;
%ADNI=newADNIMERGE(false);
load('./data/ADNIMERGE_R_201227_MRI_NM_CSF.mat','ADNI');
% load('./data/ADNIMERGE_R_201227_MRI_NM.mat','ADNI');

%% join
numImgs=size(Tdec,1);
index_err=[];
n=0;

nameFiles=string(Tdec.fsid);
RID=extractBetween(nameFiles,'S_','_');
RID=str2double(RID);
RID_unique=unique(RID);
ADNI_T=[];

% Subjects from ADNI
for i=1:numel(RID_unique)
    subject_visits=ADNI(ADNI.RID==RID_unique(i),:);
    ADNI_T=[ADNI_T;subject_visits];
end

% Subjects from Tdec
% Resizing columns in order to fit ADNIMERGE's height
aux=NaN(height(ADNI_T),1);
aux=array2table(aux);

Tdec_T=[];

for i=1:numel(Tdec.Properties.VariableNames)
    if (isa(Tdec{1,i},'double'))
        tmp=double(aux{:,1});
    elseif (isa(Tdec{1,i},'categorical'))
        tmp=categorical(aux{:,1});
    elseif (isa(Tdec{1,i},'string'))
        tmp=string(aux{:,1});
    elseif (isa(Tdec{1,i},'datetime'))
        tmp=datetime(aux{:,1},'ConvertFrom','datenum','InputFormat','dd-MM-yyyy');
    end 
    
    tmp=table(tmp);
    if (i==1)
        Tdec_T=tmp;
        Tdec_T.Properties.VariableNames{i}=Tdec.Properties.VariableNames{i};
        continue;
    end
    
    Tdec_T=[Tdec_T tmp];
    Tdec_T.Properties.VariableNames{i}=Tdec.Properties.VariableNames{i};
end

%% Checking if IMAGEUID_xml appears in ADNIMERGE
index_mri=[];

for i=1:numImgs
    index_subj=find(ADNI_T.RID==RID(i));
    IMAGEUID=extractAfter(nameFiles(i),'_I');
    IMAGEUID=str2double(IMAGEUID);
    index_tmp=index_subj(IMAGEUID==ADNI_T.IMAGEUID(index_subj));
    if ~isempty(index_tmp)
        Tdec_T(index_tmp,:)=Tdec(i,:); 
        index_mri=[index_mri;index_tmp];
    else
        CODE=Tdec.CODE(i);
        index_tmp2=index_subj(CODE==ADNI_T.VISCODE(index_subj));
        if ~isempty(index_tmp2)
            diffExamDate=abs(Tdec.EXAMDATE(i)-ADNI_T.EXAMDATE(index_tmp2));
            mask_diffExamDate=diffExamDate>200;% 200 days
            if(sum(mask_diffExamDate)==0)
                index_mri=[index_mri;index_tmp2];
                Tdec_T(index_tmp2,:)=Tdec(i,:);
                fprintf('The visit with IMAGEUID %d for RID %d is not the right one. Close visit %d for VISCODE %s.\n',IMAGEUID,RID(i),ADNI_T.IMAGEUID(index_tmp2),CODE);
            else
                fprintf(2,'The visit with IMAGEUID %d for RID %d is not the right one. Close visit %d for VISCODE %s does not match EXAMDATE.\n',IMAGEUID,RID,ADNI_T.IMAGEUID(index_tmp2),CODE);
            end
        else
            index_err=[index_err index_tmp2];
            fprintf(2,'IMAGEUID %d from RID %d does not appear in ADNIMERGE. Right IMAGEUID not found for VISCODE %s.\n',IMAGEUID,RID(i),CODE);
            n=numel(index_err);
        end
    end
end

if n~= 0
    fprintf(2,'Total images excluded from Tresults: %d.\n',n);
end

%pause

Tdec_T.Properties.VariableNames{'EXAMDATE'}='EXAMDATE_xml';
Tdec_T.Properties.VariableNames{'ICV'}='ICV_xml';
Tdec_T.Properties.VariableNames{'MMSE'}='MMSE_xml';
Tdec_T.Properties.VariableNames{'FAQ'}='FAQ_xml';
Tdec_T.Properties.VariableNames{'IMAGEUID'}='IMAGEUID_xml';

Tresults=[ADNI_T Tdec_T];

[mask_NC,Convert_NC,convTime_NC,~]= convertTime_NC(Tresults);
column=zeros(height(Tresults),1);
Convert=column;
Convert(mask_NC)=Convert_NC;
convertTime=column;
convertTime(mask_NC)=convTime_NC;

Convert=table(Convert);
convertTime=table(convertTime);

try
Tresults=[Tresults,Convert,convertTime];

% Only visits with MRI
mri=[];
if (strcmp(type,'only visits with MRI'))
    Tresults=Tresults(index_mri,:);
    mri='_mri';
else
    %Adjusting Convert and convertTime in visits with undefined DX
    Tresults=checking_conv(Tresults);
end

T_visits=height(Tresults);
T_subjects=numel(RID_unique);

name=sprintf('Tresults_%dv_%ds%s',T_visits,T_subjects,mri);

save(name,'Tresults');
catch
    % Not all elements are NC at baseline
    Tresults_NC=Tresults(mask_NC,:);
    Subj_notNCbl=numel(unique(Tresults.RID))-numel(unique(Tresults_NC.RID));
    fprintf(2,'%d subjects are not NC at baseline',Subj_notNCbl);
end
rmpath('./aux_NII_ADNIMERGE/');
rmpath('./external/lme/Qdec/');
end

function Tresults=checking_conv(Tresults)
RID=unique(Tresults.RID);
    for i=1:numel(RID)
       visits=find(Tresults.RID==RID(i));
       conv=Tresults.Convert(visits)==1;
       conv_t=Tresults.convertTime(visits)~=0;
       % Checking Convert
       if sum(conv)>0 && sum(conv)<numel(visits)
          Tresults.Convert(visits)=1; 
       end
       %Checking convertTime
       if sum(conv_t)>0 && sum(conv_t)<numel(visits)
          time=unique(Tresults.convertTime(visits));
          Tresults.convertTime(visits)=max(time);
       end
    end
end
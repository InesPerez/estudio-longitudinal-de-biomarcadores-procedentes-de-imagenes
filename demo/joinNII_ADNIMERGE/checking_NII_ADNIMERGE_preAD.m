function checking_NII_ADNIMERGE_preAD
clc;clear all;

%% Read Tresults
% [Tresults,diffIMAGEUID]=joinNII_ADNIMERGE;

% Ines Tresults test
 load('./Tresults_274v_68s_mri','Tresults');% RID 4376: baseline EXAMDATE_xml 1/6/11, EXAMDATE 19/1/12
%  load('./Tresults_147v_35s_mri','Tresults');
%  load('./Tresults_113v_24s_mri','Tresults');
%  load('./Tresults_69v_14s_mri','Tresults');% RID 232: CODE no_visit_defined, ADNI 48 | RID 985: 2 different visits
%  load('./Tresults_81v_13s_mri','Tresults');
%  load('./Tresults_301v_65s_mri','Tresults'); 
% 
% load('./Tresults_234v_64s_mri','Tresults');

% load('./Tresults_603v_111s_mri','Tresults');%RID 575: CODE No Visit Defined, ADNI 36 | RID 1190,1256: CODE ADNI2 Initial Visit-Cont Pt, ADNI 48
                                          %RID 23,68,72,127,553,734,1098 Different IMAGEUID
% load('./Tresults_1822v_394s_mri','Tresults');
% load('./Tresults_4113v_394s','Tresults');
clc;

% Only visits processed with FS
[~,index_fs]=rmmissing(Tresults.fsid);
Tresults=Tresults(~index_fs,:);

%% All visits from a subject
RID=Tresults.RID;
RID_subjs=unique(RID);

err_images=[];
for i=1:length(RID_subjs)
    %% Coherence Tmarkers
    index=find(Tresults.RID==RID_subjs(i));
    if(issorted(index)==0)
        fprintf('No sorted visits from RID %d\n',RID_subjs(i));
    end
    follow_visit=index(2:end)-index(1:end-1);
    mask_follow_visit=follow_visit~=1;
    if(sum(mask_follow_visit)>0)
        fprintf('There are jumps among visits from RID %d\n',RID_subjs(i));
    end
    
    years=Tresults.years(index);
    if(issorted(years)==0)
        fprintf(2,'No sorted years from RID %d\n',RID_subjs(i));
    end
    if(years(1)>0)
        fprintf('First visit is not baseline from RID %d\n',RID_subjs(i));
        err_images=[err_images;index];
    end
    
    %% ADNIMERGE matching??? 
    month=Tresults.M(index);

    pausar=false;
    
    % ADNI month order: problem relation NII+XML
    if(issorted(month)==0)
        fprintf(2,'No sorted years from RID %d\n',RID_subjs(i));
        pausar=true;
    end
    
    % visit time between abs(Tdec.year-ADNI.M)
    years_ADNI=month/12;
    years_ADNI=years_ADNI-years_ADNI(1);
    diff_years=abs(years-years_ADNI);
    mask_diff_years=diff_years>1;
    if(sum(mask_diff_years)>0)
        fprintf('Different visits %d from RID %d\n',...
            sum(mask_diff_years),RID_subjs(i));
        %pausar=true;
        
    end
    
    % DiffExamdate
    diffExamDate_subj=abs(Tresults.EXAMDATE(index)-Tresults.EXAMDATE_xml(index));
    mask_diffExamDate=diffExamDate_subj>200;% 200 days
   
    if(sum(mask_diffExamDate)>0)
        fprintf('Different EXAMDATA %d from RID %d\n',...
            sum(mask_diffExamDate),RID_subjs(i));
        pausar=true;
    end
    
    % diffIMAEUID
    if RID_subjs(i)==5040
        1;
    end
    diffUID_subj=abs(Tresults.IMAGEUID(index)-fsid2IMAGEUID(Tresults.fsid(index)));
    mask_diffUID=diffUID_subj>100000;  
    if(sum(mask_diffUID)>0)
        fprintf('Different IMAGEUID %d from RID %d\n',...
            sum(mask_diffUID),RID_subjs(i));
        pausar=true;
    end
    
    %% NEW CODE
    mask_diffVISCODE=Tresults.CODE(index)~=Tresults.VISCODE(index);
    if(sum(mask_diffVISCODE)>0)
        fprintf('Different VISCODE %d from RID %d\n',...
            sum(mask_diffVISCODE),RID_subjs(i));
        pausar=true;
    end
    %% 

    if(pausar)
        %pause;
    end
       
end
end


function UID=fsid2IMAGEUID(stringUID)
UID=zeros(length(stringUID),1);
for i=1:length(stringUID)
   image=extractAfter(stringUID(i),'I');
   UID(i)=str2double(image);
end

end

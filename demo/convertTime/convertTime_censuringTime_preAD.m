function convertTime_censuringTime_preAD
clc, clear all,close all;
addpath('./data');

load('Tresults_4113v_394s.mat','Tresults');
%load('Tresults_1822v_394s_mri.mat','Tresults');

load('ADNIMERGE_R_201227_MRI_NM_CSF.mat','ADNI');
figure('Name','Subjects with CSF markers','NumberTitle','off');
times(ADNI,Tresults,1);

load('ADNIMERGE_R_201227_MRI_NM.mat','ADNI');
figure('Name','Subjects without CSF markers','NumberTitle','off');
times(ADNI,Tresults,0);



end

function [convertTime,censuringTime]=times(ADNI,Tresults,csf)
RID=unique(ADNI.RID);
first=[];
subjects=[];
for i=1:numel(RID)
    n=find(Tresults.RID==RID(i));
    if ~isempty(n)
        m=numel(subjects)+1;
        first=[first;m];
        subjects=[subjects;n];
    end
end
Tresults_aux=Tresults(subjects(first),:);
index=Tresults_aux.Convert==0;

censuringTime=Tresults_aux.convertTime(index)/12;%years
convertTime=Tresults_aux.convertTime(~index)/12;%years

subplot(1,2,1);
histogram(convertTime);
title('Convert Time');

subplot(1,2,2);
histogram(censuringTime);
title('Censuring Time');

groups=categorical(Tresults.Convert(subjects),[0 1],{'sNC' 'pNC'});
CSF_subj=[];
if csf==1
    CSF_subj=' (CSF)';
end
fprintf('Converts%s: sNC: %d sujetos con %d visitas, pNC: %d sujetos con %d visitas.\n',CSF_subj,sum(groups(first)=='sNC'),sum(groups=='sNC'),...
    sum(groups(first)=='pNC'), sum(groups=='pNC'));
end
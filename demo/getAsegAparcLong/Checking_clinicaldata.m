function Checking_clinicaldata

addpath('./external/lme/Qdec/');
%% data
% % 25v_6s
% main_path='./iperez_25v';
% clinicaldata='/clinicaldata_long_25v_6s_NC.dat';
% aparclh_path='/aparc_long_lh.clinicaldata_long_25v_6s_NC.dat';
% aparcrh_path='/aparc_long_rh.clinicaldata_long_25v_6s_NC.dat';
% aseg_path='/aseg_long.clinicaldata_long_25v_6s_NC.dat';

% %234v_64s
% main_path='./fsoria_234v';
% clinicaldata='/clinicaldata_long_234v_64s_NC.dat';
% aparclh_path='/aparc_long_lh.clinicaldata_long_234v_64s_NC.dat';
% aparcrh_path='/aparc_long_rh.clinicaldata_long_234v_64s_NC.dat';
% aseg_path='/aseg_long.clinicaldata_long_234v_64s_NC.dat';

% %113v_21s
% main_path='./idelacalle_113v';
% clinicaldata='/idelacalle_prec.dat';
% aparclh_path='/aparc_long_lh.idelacalle_prec.dat';
% aparcrh_path='/aparc_long_rh.idelacalle_prec.dat';
% aseg_path='/aseg_long.idelacalle_prec.dat';

% %164v_40s
% main_path='./afernandez_164v';
% clinicaldata='/clinicaldata_long_164v_40s_NC.dat';
% aparclh_path='/aparc_long_lh.clinicaldata_long_164v_40s_NC.dat';
% aparcrh_path='/aparc_long_rh.clinicaldata_long_164v_40s_NC.dat';
% aseg_path='/aseg_long.clinicaldata_long_164v_40s_NC.dat';

% %69v_14s
% main_path='./preclinical_451v/69v_14s';
% clinicaldata='/clinicaldata_long_conversNC.dat';
% aparclh_path='/aparc_long_lh.clinicaldata_long_conversNC.dat';
% aparcrh_path='/aparc_long_rh.clinicaldata_long_conversNC.dat';
% aseg_path='/aseg_long.clinicaldata_long_conversNC.dat';

% %81v_13s
% main_path='./preclinical_451v/81v_13s';
% clinicaldata='/clinicaldata_long_preAD_81v_13s.dat';
% aparclh_path='/aparc_long_lh.clinicaldata_long_preAD_81v_13s.dat';
% aparcrh_path='/aparc_long_rh.clinicaldata_long_preAD_81v_13s.dat';
% aseg_path='/aseg_long.clinicaldata_long_preAD_81v_13s.dat';

% %301v_65s
% main_path='./preclinical_451v/301v_65s';
% clinicaldata='/clinicaldata_long_301v_65s_NC.dat';
% aparclh_path='/aparc_long_lh.clinicaldata_long_301v_65s_NC.dat';
% aparcrh_path='/aparc_long_rh.clinicaldata_long_301v_65s_NC.dat';
% aseg_path='/aseg_long.clinicaldata_long_301v_65s_NC.dat';

%603v_106s
main_path='ywu_603v';
clinicaldata='/clinical_NC_Long.dat';
aparclh_path='/aparc_long_lh.clinical_NC_Long.dat';
aparcrh_path='/aparc_long_rh.clinical_NC_Long.dat';
aseg_path='/aseg_long.clinical_NC_Long.dat';

Qdec_old=fReadQdec(strcat(main_path,'/old',clinicaldata));
Qdec_new=fReadQdec(strcat(main_path,'/new',clinicaldata));

size_old=length(Qdec_old);
size_new=length(Qdec_new);

if size_old==size_new
    
    fsid_new=Qdec_new(2:end,1);
    fsid_old=Qdec_old(2:end,1);
    
    fsidbase_new=Qdec_new(2:end,2);
    fsidbase_old=Qdec_old(2:end,2);
    
    index=[];
    
    for i=1:size_old-1
        if ~strcmp(fsid_new{i},fsid_old{i})||~strcmp(fsidbase_new{i},fsidbase_old{i})
            index=[index;i];
        end
    end
    
    clinicalok=1;
    if ~isempty(index)
        fsidbase_not_sorted=unique(fsidbase_new(index));
        for i=1:length(fsidbase_not_sorted)
            
            n_new=strcmp(fsidbase_new,fsidbase_not_sorted(i));
            fsid_subject_new=fsid_new(n_new);
            
            n_old=strcmp(fsidbase_old,fsidbase_not_sorted(i));
            fsid_subject_old=fsid_new(n_old);
            
            if length(fsid_subject_new)==length(fsid_subject_old)
                for j=1:length(fsid_subject_old)
                    aux=strcmp(fsid_subject_new,fsid_subject_old(j));
                    if isempty(fsid_subject_new(aux))
                        fprintf('Old subject %s has not been found in new clinical data.\n',fsid_subject_old{j});
                        clinicalok=0;
                    elseif length(fsid_subject_new(aux))~=1
                        fprintf('Image repeated %s in new clinical data.\n',fsid_subject_old{j});
                        clinicalok=0;
                    end
                end
            else
                fprintf(2,'Different images in old clinical data %d vs new clinical data %d for %s.\n',length(fsid_subject_old),length(fsid_subject_new),fsidbase_not_sorted{i});
                clinicalok=0;
            end
        end
        if clinicalok
            fprintf('xml names are rotated, it will be necessary to rebuild aseg and aparc tables.\n');
            
            command=sprintf('mv %s %s',strcat(main_path,aparclh_path),strcat(main_path,'/old',aparclh_path));
            disp(command);
            system(command);
            
            command=sprintf('mv %s %s',strcat(main_path,aparcrh_path),strcat(main_path,'/old',aparcrh_path));
            disp(command);
            system(command);
            
            command=sprintf('mv %s %s',strcat(main_path,aseg_path),strcat(main_path,'/old',aseg_path));
            disp(command);
            system(command);
        end
    else
        fprintf('Old aseg and aparc tables are still useful');
    end
else
    fprintf(2,'Different images in old clinical data table vs new clinical data table.\n');
end
function getAsegAparcADNILong
    % Generates files with clinical and demographic data * .dat, as well as
    % information of subcortical structures (aseg) and cortical (aparc)
    % files. This files will later be converted into tables and more
    % neuropsicological factors will be added in form of new columns.
    % It is prepared for the long. For the cross comment the long code and
    % uncomment the cross.
    
    %% Freesurfer variables
    % export FREESURFER_HOME=/usr/local/freesurfer
    % source /usr/local/freesurfer/SetUpFreeSurfer.sh
    % export SUBJECTS_DIR = /path_to_Subjects_dir
    %% data
    addpath('./aux_Aseg_aparc/');
    
    % Newton
    
    %274v_68s
    mainPath='./iperez_274v/';
    %path_subj=strcat(mainPath,'/Long_iperez_274v_68s');
    path_subj='/home/iperez/ADNI_274v_68s/Long';
    name_dat='clinicaldata_long_274v_68s_NC.dat';
    
%     %234v_64s
%     mainPath='./fsoria_234v/';
%     path_subj=strcat(mainPath,'/Long_fsoria_234v_64s');
%     %path_subj='/media/maxtor2/personal/fsoria/fsoria/ADNI_234v_64s/Long';
%     name_dat='clinicaldata_long_234v_64s_NC.dat';
    
%     %113v_21s
%     mainPath='./idelacalle_113v/';
%     path_subj=strcat(mainPath,'/Long_idelacalle_113v_21s');
%     %path_subj='/media/maxtor2/personal/idelacalle/preclinico/IMAGENESYXML/ADNI_bloque_irene/Long';
%     name_dat='idelacalle_prec.dat';
    
    %Gauss
    
%     %69v_14s
%     mainPath='./preclinical_451v/69v_14s/';
%     path_subj=strcat(mainPath,'/Long_preclinical_69v_14s');
%     %path_subj='/media/6T/personal/cplatero/preAD/FS/conversores/Long';
%     name_dat='clinicaldata_long_conversNC.dat';
    
%     %81v_13s
%     mainPath='./preclinical_451v/81v_13s/';
%     path_subj=strcat(mainPath,'/Long_preclinical_81v_13s');
%     %path_subj='/media/6T/personal/cplatero/preAD/preAD_idelacalle/Long';
%     name_dat='clinicaldata_long_preAD_81v_13s.dat';
    
%     %301v_65s
%     mainPath='./preclinical_451v/301v_65s/';
%     path_subj=strcat(mainPath,'/Long_preclinical_301v_65s');
%     %path_subj='/media/6T/personal/cplatero/preAD/FS/FS/Long';
%     name_dat='clinicaldata_long_301v_65s_NC.dat';

%     %603v_106s
%     mainPath='./ywu_603v/';
%     path_subj=strcat(mainPath,'Long_ywu_603v_106s');
%     %path_subj='/media/Elements/Gauss/ywu/NC/Long';
%     name_dat='clinical_NC_Long.dat';
    
    % Copérnico
    
%     %164v_40s
%     mainPath='./afernandez_164v/';
%     path_subj=strcat(mainPath,'/Long_afernandez_164v_40s');
% %     path_subj='/media/Maxtor/personal/afernandez/ADNI_164V_40S/Long';
% %     name_dat='clinicaldata_long_164v_40s_NC.dat';
%     name_dat='clinicaldata_long_147v_35s_NC.dat'; %active mask147v in QdecTableLong.m line:43
    
    
    xmlPath=strcat(mainPath,'xml/');
    
    %% Long
    % For this type of processing SUBJECTS_DIR must be changed to the Long
    % folder 
    
    try
        command=sprintf('mv %s %s',strcat(mainPath,name_dat),strcat(mainPath,'old/',name_dat));
        disp(command);
        system(command);
    catch
    end
    
    QdecTableLong(xmlPath,path_subj,name_dat);
    
    % generation of aseg file with subcortical structures info. The
    % --common-segs flag makes data homogenous ensuring that only the
    % segmentations obtained in all visits are used to build the aseg file.
    command= strcat('asegstats2table --qdec-long',32,name_dat,32,...
        '-t ./aseg_long.',name_dat,32,'--common-segs --skip');
    disp(command);
    system(command);
    
    % generation of aparc file with left hemisphere cortical structures
    % info.
    command= strcat('aparcstats2table --qdec-long',32,name_dat,32,...
        '-t ./aparc_long_lh.',name_dat,32,'--hemi lh --meas thickness --skip');
    disp(command);
    system(command);
    
    % generation of aparc file with right hemisphere cortical structures
    % info.
    command= strcat('aparcstats2table --qdec-long',32,name_dat,32,...
        '-t ./aparc_long_rh.',name_dat,32,'--hemi rh --meas thickness --skip');
    disp(command);
    system(command);
    
     
    rmpath('./aux_Aseg_aparc/');
    
    command=sprintf('mv %s %snew/%s',name_dat,mainPath,name_dat);
    disp(command);
    system(command);
    
    command=sprintf('mv %sxlsx %snew/%sxlsx',name_dat(1:end-3),mainPath,name_dat(1:end-3));
    disp(command);
    system(command);
    
    command=sprintf('mv aseg_long.%s %snew/aseg_long.%s',name_dat,mainPath,name_dat);
    disp(command);
    system(command);
    
    command=sprintf('mv aparc_long_lh.%s %snew/aparc_long_lh.%s',name_dat,mainPath,name_dat);
    disp(command);
    system(command);
    
    command=sprintf('mv aparc_long_rh.%s %snew/aparc_long_rh.%s',name_dat,mainPath,name_dat);
    disp(command);
    system(command);
end









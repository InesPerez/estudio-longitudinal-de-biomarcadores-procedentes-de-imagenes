function summary_bsl(raw_bsl,group,feature_names)

gr=unique(group);
num_gr=length(gr);
if(num_gr>3)
    num_gr=3;
    gr=gr(1:3);
end
fprintf('Subjects: ');
for i=1:num_gr
    fprintf(' %d (%s), ',sum(group==gr(i)),char(gr(i)));
end
fprintf('\n');
for i=1:numel(feature_names)
    fprintf('%s: ',feature_names{i});
    for j=1:num_gr
        fprintf('%s: %.2f (%.2f) (%.2f %.2f),',char(gr(j)),...
            mean(raw_bsl(group==gr(j),i),'omitnan'),std(raw_bsl(group==gr(j),i),'omitnan'),...
            min(raw_bsl(group==gr(j),i)),max(raw_bsl(group==gr(j),i)));
    end
    fprintf('\n');

end

end
